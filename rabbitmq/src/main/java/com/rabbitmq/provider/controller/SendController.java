package com.rabbitmq.provider.controller;

import com.rabbitmq.config.DirectRabbitConfig;
import com.rabbitmq.config.FanoutRabbitConfig;
import com.rabbitmq.config.TopicRabbitConfig;
import com.rabbitmq.dto.BaseMsg;
import com.rabbitmq.enums.MessageTypeEnum;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ package_msg: com.rabbitmq.provider.controller
 * @author: 谭高兵
 * @ Date: 2024/8/12
 */

@RestController
@RequestMapping("send")
public class SendController {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @GetMapping("/sendDirectMessage/{messageData}")
    public String sendDirect(@PathVariable String messageData) {
        BaseMsg baseMsg = new BaseMsg(messageData, MessageTypeEnum.DIRECT);
        //将消息携带绑定键值：TestDirectRouting 发送到交换机TestDirectExchange
        rabbitTemplate.convertAndSend(DirectRabbitConfig.DIRECT_EXCHANGE, DirectRabbitConfig.DIRECT_ROUTING_KEY, baseMsg);
        return "OK";
    }

    @GetMapping("/sendTopicMessage1/{messageData}")
    public String sendTopicMessage1(@PathVariable String messageData) {
        BaseMsg baseMsg = new BaseMsg(messageData, MessageTypeEnum.TOPIC);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.MyTopicExchange, TopicRabbitConfig.RoutingKey1, baseMsg);
        return "ok";
    }

    @GetMapping("/sendTopicMessage2/{messageData}")
    public String sendTopicMessage2(@PathVariable String messageData) {
        BaseMsg baseMsg = new BaseMsg(messageData, MessageTypeEnum.TOPIC);
        rabbitTemplate.convertAndSend(TopicRabbitConfig.MyTopicExchange, TopicRabbitConfig.RoutingKey2, baseMsg);
        return "ok";
    }

    @GetMapping("/sendFanoutMessage/{messageData}")
    public String sendFanoutMessage(@PathVariable String messageData) {
        BaseMsg baseMsg = new BaseMsg(messageData, MessageTypeEnum.FANOUT);
        rabbitTemplate.convertAndSend(FanoutRabbitConfig.FANOUT_EXCHANGE, null, baseMsg);
        return "ok";
    }

    // TODO 死信队列、延时队列 实现
}
