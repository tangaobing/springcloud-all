package com.rabbitmq.consumer;

import com.rabbitmq.config.DirectRabbitConfig;
import com.rabbitmq.config.FanoutRabbitConfig;
import com.rabbitmq.config.TopicRabbitConfig;
import com.rabbitmq.dto.BaseMsg;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @ package_msg: com.rabbitmq.consumer
 * @author: 谭高兵
 * @ Date: 2024/8/12
 */

@Component
public class ConsumerListener {

    @RabbitHandler()
    //监听的队列名称 TestDirectQueue
    @RabbitListener(queues = DirectRabbitConfig.DIRECT_QUEUE_NAME)
    public void directConsumer(BaseMsg testMessage) throws InterruptedException {
        System.out.println("DirectReceiver消费者收到消息  : " + testMessage.toString());
        System.out.println("==============================");
        Thread.sleep(10000);
    }

    @RabbitHandler()
    @RabbitListener(queues = TopicRabbitConfig.queue_woman)
    public void topConsumer1(BaseMsg testMessage) {
        System.out.println("topic.woman消费者收到消息  : " + testMessage.toString());
        System.out.println("==============================");
    }

    @RabbitHandler()
    @RabbitListener(queues = TopicRabbitConfig.queue_man)
    public void topConsumer(BaseMsg testMessage) {
        System.out.println("topic.man消费者收到消息  : " + testMessage.toString());
        System.out.println("==============================");
    }

    @RabbitHandler()
    @RabbitListener(queues = FanoutRabbitConfig.FANOUT_QUEUEA_NAME)
    public void fanoutA(BaseMsg testMessage) {
        System.out.println("fanout.A消费者收到消息  : " + testMessage.toString());
        System.out.println("==============================");
    }

    @RabbitHandler()
    @RabbitListener(queues = FanoutRabbitConfig.FANOUT_QUEUEB_NAME)
    public void fanoutB(BaseMsg testMessage) {
        System.out.println("fanout.B消费者收到消息  : " + testMessage.toString());
        System.out.println("==============================");
    }

    @RabbitHandler()
    @RabbitListener(queues = FanoutRabbitConfig.FANOUT_QUEUEC_NAME)
    public void fanoutC(BaseMsg testMessage) {
        System.out.println("fanout.C消费者收到消息  : " + testMessage.toString());
        System.out.println("==============================");
    }
}
