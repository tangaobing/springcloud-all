package com.rabbitmq.enums;

/**
 * @ package_msg: com.rabbitmq.enums
 * @author: 谭高兵
 * @ Date: 2024/8/12
 */
public enum MessageTypeEnum {

    DIRECT("direct"),
    TOPIC("topic"),
    FANOUT("fanout"),
    HEADERS("headers");

    private String type;

    MessageTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
