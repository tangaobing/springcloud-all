package com.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ package_msg: com.rabbitmq.config
 * @author: 谭高兵
 * @ Date: 2024/8/12
 */
@Configuration
public class TopicRabbitConfig {
    //绑定键
    public final static String queue_man = "topic.man";
    public final static String queue_woman = "topic.woman";
    public final static String MyTopicExchange = "MyTopicExchange";

    public final static String RoutingKey2 = "topic.#";
    public final static String RoutingKey1 = "topic.man";

    @Bean
    public Queue firstQueue() {
        return new Queue(TopicRabbitConfig.queue_man);
    }

    @Bean
    public Queue secondQueue() {
        return new Queue(TopicRabbitConfig.queue_woman, true);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("MyTopicExchange");
    }


    //将firstQueue和topicExchange绑定,而且绑定的键值为topic.man
    //这样只要是消息携带的路由键是topic.man,才会分发到该队列
    @Bean
    Binding bindingExchangeMessage() {
        return BindingBuilder.bind(firstQueue()).to(exchange()).with(RoutingKey1);
    }

    //将secondQueue和topicExchange绑定,而且绑定的键值为用上通配路由键规则topic.#
    // 这样只要是消息携带的路由键是以topic.开头,都会分发到该队列
    @Bean
    Binding bindingExchangeMessage2() {
        return BindingBuilder.bind(secondQueue()).to(exchange()).with(RoutingKey2);
    }
}
