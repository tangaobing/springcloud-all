package com.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ package_msg: com.rabbitmq.config
 * @author: 谭高兵
 * @ Date: 2024/8/12
 */
@Configuration
public class FanoutRabbitConfig {

    /**
     * 创建三个队列 ：fanout.A   fanout.B  fanout.C
     * 将三个队列都绑定在交换机 fanoutExchange 上
     * 因为是扇型交换机, 路由键无需配置,配置也不起作用
     */

    public static final String FANOUT_EXCHANGE = "MyFanoutExchange";

    public static final String FANOUT_QUEUEA_NAME = "fanout.A";
    public static final String FANOUT_QUEUEB_NAME = "fanout.B";
    public static final String FANOUT_QUEUEC_NAME = "fanout.C";


    @Bean
    public Queue queueA() {
        return new Queue(FANOUT_QUEUEA_NAME, true);
    }

    @Bean
    public Queue queueB() {
        return new Queue(FANOUT_QUEUEB_NAME);
    }

    @Bean
    public Queue queueC() {
        return new Queue(FANOUT_QUEUEC_NAME);
    }

    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange(FANOUT_EXCHANGE);
    }

    @Bean
    Binding bindingExchangeA() {
        return BindingBuilder.bind(queueA()).to(fanoutExchange());
    }

    @Bean
    Binding bindingExchangeB() {
        return BindingBuilder.bind(queueB()).to(fanoutExchange());
    }

    @Bean
    Binding bindingExchangeC() {
        return BindingBuilder.bind(queueC()).to(fanoutExchange());
    }
}
