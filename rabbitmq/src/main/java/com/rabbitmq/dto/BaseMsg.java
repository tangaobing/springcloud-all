package com.rabbitmq.dto;

import com.rabbitmq.enums.MessageTypeEnum;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @ package_msg: com.rabbitmq.dto
 * @author: 谭高兵
 * @ Date: 2024/8/12
 */
@Data
@ToString
public class BaseMsg implements Serializable {

    private String messageId;

    private String messageData;

    private String createTime;

    private MessageTypeEnum messageType;

    public BaseMsg() {
    }

    public BaseMsg(String messageData, MessageTypeEnum messageType) {
        this.messageId = String.valueOf(UUID.randomUUID());
        this.createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.messageData = messageData;
        this.messageType = messageType;
    }
}
