package com.wayto.plugin;

import com.alibaba.nacos.plugin.auth.api.IdentityContext;
import com.alibaba.nacos.plugin.auth.api.Permission;
import com.alibaba.nacos.plugin.auth.api.Resource;
import com.alibaba.nacos.plugin.auth.constant.ActionTypes;
import com.alibaba.nacos.plugin.auth.exception.AccessException;
import com.alibaba.nacos.plugin.auth.spi.server.AuthPluginService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @ package_msg: com.wayto.plugin
 * @author: 谭高兵
 * @ Date: 2024/8/11
 */
public class MyNacosAuthPlugin implements AuthPluginService {
    public Collection<String> identityNames() {
        List<String> list = new ArrayList<>();
        list.add("username");
        list.add("password");
        list.add("accessToken");
        return list;
    }

    public boolean enableAuth(ActionTypes actionTypes, String s) {
        return true;
    }

    public boolean validateIdentity(IdentityContext identityContext, Resource resource) {
        return false;
    }

    public Boolean validateAuthority(IdentityContext identityContext, Permission permission) {
        return null;
    }

    public String getAuthServiceName() {
        return "MyNacosAuthPlugin";
    }
}
