package com.wayto.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.security.Principal;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/03/23 16:04
 */
@Component
public class MyFilter implements GlobalFilter, Ordered {

	/***
	 * 自定义过滤器
	 * @param exchange
	 * @param chain
	 * @return
	 */
	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		Mono<Principal> principal = exchange.getPrincipal();
		/***
		 * 过滤逻辑
		 *
		 * 登录服务颁发token
		 * 网关鉴权token
		 *
		 */
		return chain.filter(exchange);
	}

	/***
	 * 定义过滤器的顺序
	 * @return
	 */
	@Override
	public int getOrder() {
		return 0;
	}
}
