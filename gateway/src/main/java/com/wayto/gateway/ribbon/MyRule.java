package com.wayto.gateway.ribbon;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.Server;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/03/23 15:19
 */
@Component
public class MyRule extends AbstractLoadBalancerRule {

	@Override
	public void initWithNiwsConfig(IClientConfig iClientConfig) {

	}

	/****
	 * 自定义负载均衡策略
	 * 随机轮询
	 * @param key
	 * @return
	 */
	@Override
	public Server choose(Object key) {
		List<Server> reachableServers = this.getLoadBalancer().getReachableServers();
		System.out.println(key.toString());
		int index = (int) (Math.random() * reachableServers.size());
		return reachableServers.get(index);
	}
}
