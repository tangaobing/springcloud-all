package com.wayto.gateway.route;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/03/23 15:52
 */
@Configuration
public class MyRoute {

	/***
	 * 自定义路由
	 * @param routeBuilder
	 * @return
	 */
	@Bean
	public RouteLocator routeLocator(RouteLocatorBuilder routeBuilder) {
		RouteLocator routeLocator = routeBuilder.routes().route("server", p ->
				p.path("/server/**")
						.filters(f ->
								f.stripPrefix(1)
						)
						.uri("lb://nacos-server")
		).build();
		return routeLocator;
	}
}
