package com.wayto.activiti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
// 导入 spring xml 配置
@ImportResource("classpath:bean/bean.xml")
public class SpringActivitiApplication {

	/****
	 * TODO
	 * 	工作流引擎学习没有头绪，回去看视频学习下
	 *
	 *
	 * @param args
	 */

	public static void main(String[] args) {
		SpringApplication.run(SpringActivitiApplication.class, args);
	}
}
