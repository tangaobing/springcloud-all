package com.wayto.zookeeper;

import com.wayto.zookeeper.api.ZKUtil;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/02/27 13:49
 */
public class ZKTest {

	ZooKeeper zooKeeper;

	String address = "10.10.1.33";

	@Before
	public void init() {
		zooKeeper = ZKUtil.zkConnection(address);
	}

	@After
	public void close() throws InterruptedException {
		zooKeeper.close();
	}

	@Test
	public void test() throws KeeperException, InterruptedException {
		boolean lol = ZKUtil.existNode(zooKeeper, "/LOL", "001");
		System.out.println("boolean  " + lol);
		List<String> node = ZKUtil.getNode(zooKeeper, "/", false);
		System.out.println(node);
	}
}
