package com.wayto.zookeeper.lock;

import com.wayto.zookeeper.api.ZKUtil;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import java.util.concurrent.TimeUnit;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/03/03 14:21
 */
public class DefaultZkLock extends BaseZKLock implements IZKLock {

	private static final String LOCK_NAME = "lock-";

	protected DefaultZkLock(ZooKeeper zooKeeper, String basePath, String lockName) {
		super(zooKeeper, basePath, lockName);
	}

	/***
	 * 创建一个锁对象
	 * @param address 连接地址
	 * @param basePath  锁对象创建的基础znode
	 * @return
	 */
	public static DefaultZkLock LockFactory(String address, String basePath) {
		return new DefaultZkLock(ZKUtil.zkConnection(address), basePath, LOCK_NAME);
	}

	@Override
	public boolean lock() {
		return tryLock(0L, 0L, "");
	}

	@Override
	public boolean lock(long time, TimeUnit timeUnit) {
		/****
		 * TODO
		 */
		return tryLock(time, 0L, "");
	}

	@Override
	public boolean unlock() {
		try {
			deleteNode(0);
		} catch (KeeperException e) {
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
