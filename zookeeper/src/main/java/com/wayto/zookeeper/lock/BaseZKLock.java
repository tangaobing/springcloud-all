package com.wayto.zookeeper.lock;

import com.wayto.zookeeper.api.MyZkException;
import com.wayto.zookeeper.api.ZKUtil;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/03/03 14:20
 */
public class BaseZKLock {
	private ZooKeeper zooKeeper;
	private String path;
	private String basePath;
	private String lockName;
	private final int RETRY_COUNT = 10;
	private final String NODE_SPLIT = "/";
	private final String TEMP = "";

	protected BaseZKLock(ZooKeeper zooKeeper, String basePath, String lockName) {
		this.zooKeeper = zooKeeper;
		this.basePath = basePath;
		this.path = basePath + NODE_SPLIT + lockName;
		this.lockName = lockName;
	}

	public void deleteNode(int version) throws KeeperException, InterruptedException {
		ZKUtil.deleteNode(zooKeeper, path, version);
	}

	public void createNode(String data, Object ctx) {
		ZKUtil.createNode(zooKeeper, path, data, ctx);
	}

	public boolean waitForLock(long startMillis, Long millisToWait, String data) throws MyZkException {
		boolean isDelete = false;

		String tempSortNode = ZKUtil.createTempSortNode(zooKeeper, path, data);
		List<String> list = ZKUtil.getNode(zooKeeper, basePath, false);
		sortedNode(list);
		int index = list.indexOf(getSequence(tempSortNode));
		// 节点创建失败 需要重试
		if (index < 0) {
			throw new MyZkException("节点创建失败");
		}
		boolean isGetLock = index == 0 ? true : false;
		// 成功获取锁
		if (isGetLock) {
			return true;
		} else {
			// 没有获取到锁, 监听上一个比自己小的节点
			String lastNode = list.get(index - 1);
			/***
			 *  使用watcher 监听当前节点，如果当前节点存在就等待，如果当前节点不存在就
			 *  执行后续程序
			 */
			while (! isDelete) {
				isDelete = ZKUtil.existNode(zooKeeper, lastNode, lastNode) ? false : true;
			}
		}
		return true;
	}

	/***
	 * 节点创建失败 需要重试 重试次数位10次
	 * @param startMillis
	 * @param millisToWait
	 * @param data
	 * @return
	 */
	public boolean tryLock(long startMillis, Long millisToWait, String data) {
		int count = 0;
		while (count < RETRY_COUNT) {
			try {
				return waitForLock(startMillis, millisToWait, data);
			} catch (MyZkException e) {
				count++;
				tryLock(startMillis, millisToWait, data);
				e.printStackTrace();
			}
		}
		System.out.println("系统出了问题");
		return false;
	}

	private void sortedNode(List<String> list) {
		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.parseInt(getSequence(o1)) - Integer.parseInt(getSequence(o2));
			}
		});
	}

	private String getSequence(String tempSortNode) {
		String replace = tempSortNode.replace(lockName, TEMP);
		return replace;
	}

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("00000012");
		list.add("0017");
		list.add("002");
		list.add("0042");
		list.add("0052");
		list.add("0000000001");
		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.parseInt(o1) - Integer.parseInt(o2);
			}
		});
		System.out.println(list);
	}
}
