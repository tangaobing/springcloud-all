package com.wayto.zookeeper.lock;

import java.util.concurrent.TimeUnit;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/02/26 14:30
 */
public interface IZKLock {
	/***
	 * 定义一个zookeeper 分布式锁接口
	 *
	 * 接口具备功能 :
	 *		1、获取锁 : 获取锁，如果没有就建立监听 watcher等待
	 *		2、获取锁 : 获取锁，如果没有就建立监听 watcher等待，另外设置超时时间
	 *		3、释放锁
	 */
	boolean lock();

	boolean lock(long time, TimeUnit timeUnit);

	boolean unlock();
}
