package com.wayto.zookeeper.api;

import org.apache.commons.lang.StringUtils;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.util.concurrent.CountDownLatch;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/02/26 16:23
 */
public class WatchCallBack implements Watcher, AsyncCallback.DataCallback, AsyncCallback.StatCallback, AsyncCallback.StringCallback {

	CountDownLatch cd = new CountDownLatch(1);
	DefaultWatchState watchState;

	public DefaultWatchState getWatchState() {
		return watchState;
	}

	public void setWatchState(DefaultWatchState watchState) {
		this.watchState = watchState;
	}

	public CountDownLatch getCd() {
		return cd;
	}

	public void setCd(CountDownLatch cd) {
		this.cd = cd;
	}

	@Override
	public void processResult(int rc, String path, Object ctx, byte[] data, Stat stat) {
		if (watchState == null) {
			/***
			 * TODO
			 * 单例
			 */
			watchState = new DefaultWatchState();
		}
		/***
		 *  DataCallback
		 */
		if (stat != null) {
			cd.countDown();
		}
		watchState.setMessage(new String(data));
	}

	@Override
	public void processResult(int rc, String path, Object ctx, Stat stat) {
		if (watchState == null) {
			/***
			 * TODO
			 * 单例
			 */
			watchState = new DefaultWatchState();
		}
		/***
		 * StatCallback
		 */
		if (stat != null) {
			watchState.setMessage("存在节点");
			watchState.setRc(rc);
			watchState.setFlag(true);
			cd.countDown();
		} else {
			watchState.setMessage("节点不存在");
			watchState.setRc(rc);
			cd.countDown();
		}
	}

	@Override
	public void process(WatchedEvent event) {
		/***
		 * watcher
		 */
		Event.KeeperState state = event.getState();
		if (watchState == null) {
			/***
			 * TODO
			 * 单例
			 */
			watchState = new DefaultWatchState();
		}
		switch (state) {
			case Unknown:
				break;
			case Disconnected:
				watchState.setMessage("Disconnected");
				break;
			case NoSyncConnected:
				break;
			case SyncConnected:
				watchState.setMessage("SyncConnected");
				break;
			case AuthFailed:
				break;
			case ConnectedReadOnly:
				break;
			case SaslAuthenticated:
				break;
			case Expired:
				break;
			case Closed:
				break;
		}

		Event.EventType type = event.getType();
		switch (type) {
			case None:
				watchState.setMessage("None");
				watchState.setFlag(false);
				break;
			case NodeCreated:
				watchState.setMessage("NodeCreated");
				watchState.setFlag(true);
				cd.countDown();
				break;
			case NodeDeleted:
				watchState.setFlag(false);
				cd.countDown();
				break;
			case NodeDataChanged:
				break;
			case NodeChildrenChanged:
				break;
			case DataWatchRemoved:
				watchState.setFlag(false);
				cd.countDown();
				break;
			case ChildWatchRemoved:
				watchState.setFlag(false);
				cd.countDown();
				break;
			case PersistentWatchRemoved:
				break;
		}
	}

	@Override
	public void processResult(int rc, String path, Object ctx, String name) {

		/****
		 * StringCallBack
		 */
		if (watchState != null) {

			watchState.setRc(rc);

		}
	}
}
