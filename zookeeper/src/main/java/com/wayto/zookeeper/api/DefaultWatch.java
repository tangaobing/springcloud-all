package com.wayto.zookeeper.api;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import java.util.concurrent.CountDownLatch;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/02/26 18:06
 */
public class DefaultWatch implements Watcher {

	private CountDownLatch countDownLatch;

	private DefaultWatchState watchState;

	public DefaultWatch(){}

	public DefaultWatch (CountDownLatch countDownLatch) {
		this.countDownLatch = countDownLatch;
	}

	public DefaultWatch (CountDownLatch countDownLatch,  DefaultWatchState watchState) {
		this.countDownLatch = countDownLatch;
		this.watchState = watchState;
	}

	public DefaultWatchState getWatchState(){
		return watchState;
	}

	/***
	 * 用于初始话zookeeper
	 * @param event
	 */
	@Override
	public void process(WatchedEvent event) {
		Event.KeeperState state = event.getState();
		if (watchState == null) {
			watchState = new DefaultWatchState();
		}
		switch (state) {
			case Unknown:
				break;
			case Disconnected:
				watchState.setMessage("Disconnected");
				break;
			case NoSyncConnected:
				break;
			case SyncConnected:
				countDownLatch.countDown();
				watchState.setMessage("SyncConnected");
				break;
			case AuthFailed:
				break;
			case ConnectedReadOnly:
				break;
			case SaslAuthenticated:
				break;
			case Expired:
				break;
			case Closed:
				break;
		}

		Event.EventType type = event.getType();
		switch (type) {
			case None:
				break;
			case NodeCreated:
				watchState.setMessage("NodeCreated");
				break;
			case NodeDeleted:
				break;
			case NodeDataChanged:
				break;
			case NodeChildrenChanged:
				break;
			case DataWatchRemoved:
				break;
			case ChildWatchRemoved:
				break;
			case PersistentWatchRemoved:
				break;
		}
	}
}
