package com.wayto.zookeeper.api;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @program: eureka-server
 * @description: zookeeper  API 初版
 * @author: tgb
 * @create: 2021/02/26 14:28
 */
public class ZKUtil {

	private static final int SESSION_TIME_OUT = 2000;

	/***
	 * 新建一个zookeeper 连接
	 * @param address  连接地址
	 * @return
	 */
	public static ZooKeeper zkConnection(String address) {
		ZooKeeper zooKeeper = null;
		CountDownLatch countDownLatch = new CountDownLatch(1);
		try {
			DefaultWatch watch = new DefaultWatch(countDownLatch);
			zooKeeper = new ZooKeeper(address, SESSION_TIME_OUT, watch);
			countDownLatch.await();
			System.out.println("zookeeper 对象创建成功  " + watch.getWatchState());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return zooKeeper;
	}

	/***
	 * 判断节点是否存在
	 * @param zooKeeper
	 * @param pathNode
	 * @param ctx
	 * @return
	 */
	public static boolean existNode(ZooKeeper zooKeeper, String pathNode, Object ctx) {
		WatchCallBack watchCallBack = new WatchCallBack();
		watchCallBack.setCd(new CountDownLatch(1));
		zooKeeper.exists(pathNode, watchCallBack, watchCallBack, ctx);
		try {
			watchCallBack.getCd().await();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		DefaultWatchState watchState = watchCallBack.getWatchState();
		return watchState.getFlag();
	}

	/***
	 * 创造节点
	 * @param zooKeeper
	 * @param pathNode
	 * @param data
	 * @param ctx
	 * @return
	 */
	public static String createNode(ZooKeeper zooKeeper, String pathNode, String data, Object ctx) {
		String back = "";
		try {
			List<ACL> list = new ArrayList<>();
			list.add(new ACL());
			back = zooKeeper.create(pathNode, data.getBytes(StandardCharsets.UTF_8), list, CreateMode.EPHEMERAL);
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return back;
	}

	/***
	 * 创建一个临时有序节点
	 * @param zooKeeper
	 * @param pathNode
	 * @param data
	 * @param ctx
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public static String createTempSortNode(ZooKeeper zooKeeper, String pathNode, String data) {
		String path = "";
		List<ACL> list = new ArrayList<>();
		list.add(new ACL());
		try {
			path = zooKeeper.create(pathNode, data.getBytes(StandardCharsets.UTF_8), list, CreateMode.EPHEMERAL_SEQUENTIAL);
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return path;
	}

	/***
	 * 修改节点内的数据
	 * @param zooKeeper
	 * @param pathNode
	 * @param data
	 * @param ctx
	 * @param version
	 * @return
	 */
	public static boolean setData(ZooKeeper zooKeeper, String pathNode, String data, Object ctx, int version) {
		WatchCallBack watchCallBack = new WatchCallBack();
		zooKeeper.setData(pathNode, data.getBytes(StandardCharsets.UTF_8), version, watchCallBack, ctx);
		System.out.println("setDate rc = " + watchCallBack.getWatchState().getRc());
		return true;
	}

	/****
	 * 获取子节点
	 */
	public static List<String> getNode(ZooKeeper zooKeeper, String pathNode, boolean watch) {
		WatchCallBack watchCallBack = new WatchCallBack();
		List<String> data = new ArrayList<>();
		try {
			if (watch) {
				data = zooKeeper.getChildren(pathNode, watchCallBack);
			} else {
				data = zooKeeper.getChildren(pathNode, watch);
			}
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return data;
	}

	/***
	 * 获取节点内的数据
	 */
	public static DefaultWatchState getData(ZooKeeper zooKeeper, String pathNode, String data, Object ctx) throws InterruptedException {
		WatchCallBack watchCallBack = new WatchCallBack();
		zooKeeper.getData(pathNode, watchCallBack, watchCallBack, ctx);
		watchCallBack.setCd(new CountDownLatch(1));
		watchCallBack.getCd().wait();
		return watchCallBack.getWatchState();
	}

	public static void deleteNode(ZooKeeper zooKeeper, String pathNode, int version) throws KeeperException, InterruptedException {
		zooKeeper.delete(pathNode, version);
	}
}
