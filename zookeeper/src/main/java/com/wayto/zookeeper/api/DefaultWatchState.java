package com.wayto.zookeeper.api;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/02/27 11:12
 */
public class DefaultWatchState {
	/**
	 * 分装回收defaultWatch监听的结果
	 * 根据业务场景实现
	 */
	String message;

	boolean flag;

	/***
	 * callback  的 回调 结果
	 */
	int rc;

	public int getRc() {
		return rc;
	}

	public void setRc(int rc) {
		this.rc = rc;
	}

	public boolean isFlag() {
		return flag;
	}

	public boolean getFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "DefaultWatchState{" +
				"message='" + message + '\'' +
				", flag=" + flag +
				", rc=" + rc +
				'}';
	}
}
