package com.wayto.zookeeper.api;

/**
 * @program: eureka-server
 * @description:
 * @author: tgb
 * @create: 2021/03/03 15:28
 */
public class MyZkException extends  Exception{

	public MyZkException(String message) {
		super(message);
	}
}
