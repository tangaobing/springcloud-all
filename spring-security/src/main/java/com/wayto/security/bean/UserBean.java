package com.wayto.security.bean;

import java.io.Serializable;

/**
 * @program: eureka-server
 * @description: userBean
 * @author: tgb
 * @create: 2020/11/03 14:00
 */
public class UserBean implements Serializable {

	private static final long serialVersionUID = -814031798199130344L;

	private String username;

	private String password;

	private String role;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
