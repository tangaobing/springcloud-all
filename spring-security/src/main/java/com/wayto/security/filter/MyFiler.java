package com.wayto.security.filter;

import com.google.code.kaptcha.Constants;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @program: eureka-server
 * @description: 过滤器
 * @author: tgb
 * @create: 2020/11/03 14:39
 */
@Component
public class MyFiler implements Filter{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	// 拦截验证码请求
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String uri = request.getRequestURI();

		// 判定的post 请求为大写
		String method = request.getMethod();

		if (uri.equals("/login/au") && method.equalsIgnoreCase("post")) {
			// 获取存储的二维码
			String attributeCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);

			String code = request.getParameter("code").trim();

			if (StringUtils.isEmpty(code)) {
				throw new RuntimeException("验证码不能为空");
			}

			if(code.equalsIgnoreCase(attributeCode)) {

				System.out.println("验证通过");

			}
			System.out.println(attributeCode);
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {

	}
}
