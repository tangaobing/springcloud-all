package com.wayto.security.service;

import com.wayto.security.bean.UserBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @program: eureka-server
 * @description: 用户校验处理
 * @author: tgb
 * @create: 2020/11/02 10:26
 */

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private JdbcUserDetailsManager manager;

	@Autowired
	private PasswordEncoder passwordEncoder;


	/***
	 * 根据用户名查询该用户,对用户进行处理
	 *
	 * spring-security 自带的user 建表语句
	 *
	 * org/springframework/security/spring-security-core/5.3.4.RELEASE/spring-security-core-5.3.4.RELEASE.jar!/org/springframework/security/core/userdetails/jdbc/users.ddl
	 *
	 * @param username 用户名称
	 * @return
	 * @throws UsernameNotFoundException
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

			return manager.loadUserByUsername(username);

		// UserDetails userDetails = manager.loadUserByUsername(username);

		/***
		 *
			 修改密码 根据系统传输的token查到用户名修改密码
			 manager.changePassword();
			 加载用户，根据用户名查找用户
			 UserDetails userDetails = manager.loadUserByUsername(username);
		 */
	}

	/***
	 * 注册新用户
	 * 正常业务需要重写 User,初步设想可以使用两张表，一张存储原生user，一张存储自定义user
	 *
	 *  对页面传过来的参数进行封装
	 *
	 *  封装可以使用User的build方式,使用build 添加角色会在角色字段前自动添加 ROLE_ 字符串，方便后续的权限颗粒度访问功能的实现
	 *
	 *  不使用该方式，推测需要自动添加 ROLE_  字符串
	 *
	 *
	 */
	public void registerUser(UserBean user) {
		String password = passwordEncoder.encode(user.getPassword());
		manager.createUser(User.withUsername(user.getUsername())
				.password(password)
		.roles(user.getRole()).build());
	}

}
