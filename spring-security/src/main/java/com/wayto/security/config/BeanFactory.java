package com.wayto.security.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @program: eureka-server
 * @description: 创建Bean
 * @author: tgb
 * @create: 2020/11/02 11:29
 */

@Configuration
public class BeanFactory {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/***
	 * spring-security 自带处理jdbc工具
	 * 添加jdbc用户管理
	 * @return
	 */
	@Bean
	public JdbcUserDetailsManager createUserManager() {
		JdbcUserDetailsManager manager = new JdbcUserDetailsManager();
		manager.setDataSource(dataSource);
		manager.setJdbcTemplate(jdbcTemplate);
		return manager;
	}

	/***
	 * 添加加密方式， 否则登陆校验不通过
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/****
	 * 创建一个 DefaultKaptcha 对象
	 * 内部依赖对象采用 new 对象方式生成
	 * @return
	 */
	@Bean
	public DefaultKaptcha createDefaultKaptcha() {
		DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
		Properties properties = new Properties();
		properties.setProperty("kaptcha.border", "yes");
		properties.setProperty("kaptcha.border.color", "105,179,90");
		properties.setProperty("kaptcha.textproducer.font.color", "blue");
		properties.setProperty("kaptcha.image.width", "200");
		properties.setProperty("kaptcha.image.height", "125");
		properties.setProperty("kaptcha.textproducer.font.size", "45");
		properties.setProperty("kaptcha.session.key", "code");
		properties.setProperty("kaptcha.textproducer.char.length", "4");
		properties.setProperty("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑");
		defaultKaptcha.setConfig(new Config(properties));
		return defaultKaptcha;
	}

	/****
	 * 权限继承
	 * @return
	 */
	@Bean
	public RoleHierarchy roleHierarchy() {

		RoleHierarchyImpl impl = new RoleHierarchyImpl();

		/****
		 *  admin 继承了 user 权限
		 *
		 *  admin的权限大于user权限
		 *
		 */
		impl.setHierarchy("ROLE_admin > ROLE_user");
		return impl;

	}


}
