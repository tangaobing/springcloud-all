package com.wayto.security.config;

import com.wayto.security.filter.MyFiler;
import com.wayto.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.DefaultHttpFirewall;

/**
 * @program: eureka-server
 * @description: security配置
 * @author: tgb
 * @create: 2020/10/29 16:13
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userService;

	@Autowired
	private MyFiler filer;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// 开启权限校验
		http.authorizeRequests()

				// 手动添加权限 (查看内部代码, 添加的角色信息已经自动拼接上了 ROLE_ ) 此配置需要配置在最开始步骤
				/*.antMatchers("/user/**").hasAnyRole("user")
				.antMatchers("/admin/**").hasAnyRole("admin")
				.antMatchers("/guest/**").hasAnyRole("guest")*/
				// 设置未实现登陆之前的可访问路径
				.antMatchers("/login", "/failure", "/verification", "/register").permitAll()
				.anyRequest().authenticated()
				.and()

				// 添加前置处理器
				.addFilterBefore(filer, UsernamePasswordAuthenticationFilter.class)

				.formLogin()

				// 指定跳转登陆页的路径
				.loginPage("/login")

				// 映射配置客户端form表单对应的name属性
				.usernameParameter("MyUN")
				.passwordParameter("MyPW")

				// 指定自定义表单的请求路径
				.loginProcessingUrl("/login/au")

				// 登陆失败访问路径,与失败处理器优先使用登陆失败处理器
//				.failureUrl("/failure")

				// 默认访问成功的跳转路径
				.defaultSuccessUrl("/welcome", true)

				// 登陆失败处理器
				.failureHandler((request, response, authException) -> {
					//	authException.printStackTrace();

					// 此处为 post 请求
					response.setHeader("Location", "/Day06_01/Csertlet");
					response.setStatus(302);
					response.sendRedirect("/failure");
//					request.getRequestDispatcher("/failure").forward(request, response);

				})

				// 登陆成功处理器
				//.successHandler(null)

				.and()

		// 退出登陆处理器
		//.logout()
		//.addLogoutHandler(null)

		/***
		 *  spring-security 自带的remember me 功能，此处记录个小问题，学完会话管理后自己实现
		 *
		 *  待实现功能
		 *
		 * 		remember me
		 *  	踢掉其他已登录的用户
		 *  	禁止其他终端登陆
		 *
		 * 		 // .rememberMe()
		 *
		 * */
		;

		// 关闭csrf 防止跨域伪造站点攻击
		http.csrf().disable();
	}

	/***
	 * 自定义用户
	 * @param auth
	 * @throws Exception
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// 内存中存储用户数据
		/*auth.inMemoryAuthentication()
				.withUser("tan")
				.password(passwordEncoder.encode("123"))
				// 必须传递角色
				.roles("admin")
				.and()
				.withUser("gao")
				.password(passwordEncoder.encode("123"))
				.roles("admin")
				.and();*/

		// 连接数据库使用 注入UserDetailsService
		auth.userDetailsService(userService).and();
	}


	// 自定义用户的第二种方式
	/*@Override
	@Bean
	protected UserDetailsService userDetailsService() {
		new InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder>().getUserDetailsService().createUser();
	}*/

	@Override
	public void configure(WebSecurity web) throws Exception {
		// 配置静态资源的允许访问
		web.ignoring().mvcMatchers("/static");

		/***
		 * 对请求路径
		 * 响应数据
		 * FirewalledRequest getFirewalledRequest(HttpServletRequest request)
		 *
		 * HttpServletResponse getFirewalledResponse(HttpServletResponse response);
		 */
		web.httpFirewall(new DefaultHttpFirewall());
	}
}
