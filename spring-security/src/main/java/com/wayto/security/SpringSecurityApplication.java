package com.wayto.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@ServletComponentScan
@EnableWebSecurity
@EnableGlobalMethodSecurity (prePostEnabled = true,securedEnabled = true)
public class SpringSecurityApplication {

	/***
	 * security 默认用户名为user
	 *
	 * ServletComponentScan  开启 filter 扫描
	 *
	 * @EnableWebSecurity 开启web security 安全认证
	 *
	 * @EnableGlobalMethodSecurity (prePostEnabled = true,securedEnabled = true)  开启方法上的颗粒度权限
	 *
	 *
	 * 	访问方法需要具有的权限
	 * @Secured({"ROLE_admin","ROLE_user"})
	 *
	 * 	单个权限才能访问
	 * @PreAuthorize("hasRole('ROLE_user')")
	 *
	 *	多个权限可以访问
	 * @PreAuthorize("haAnyRole('ROLE_admin','ROLE_user')")
	 *
	 * 	同时具有2个权限才能访问
	 * @PreAuthorize("hasRole('ROLE_admin') AND hasRole('ROLE_user')")
	 *
	 * 	返回值判断是否具有权限
	 * @PostAuthorize("returnObject==1")
	 *
	 *
	 * 	注 : 注解配置与代码中配置
	 * 				antMatchers("/user/**").hasAnyRole("user") 有冲突
	 * 				二者之间选其一
	 *
	 *
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);
	}
}
