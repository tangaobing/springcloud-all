package com.wayto.security.controller;

import com.wayto.security.bean.UserBean;
import com.wayto.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @program: eureka-server
 * @description: 登陆测试
 * @author: tgb
 * @create: 2020/10/29 16:12
 */

@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@GetMapping ("/login")
	public String login(@RequestParam(required = false) String error) {
		/**
		 * 前后端分离的情况下，可以使用nginx实现首次访问登陆页面
		 */
		return "login";
	}

	@GetMapping("/welcome")
	public String welcome() {

		// 获取用户权限信息 SecurityContextHolder.getContext().getAuthentication()
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		Object principal = authentication.getPrincipal();

		System.out.println(principal);

		return "welcome";
	}

	@GetMapping("/failure")
	public String failure() {
		System.out.println("失败跳转");
		return "failure";
	}


	@PostMapping("/register")
	public String register(UserBean userBean) {
		userService.registerUser(userBean);
		return "login";
	}

}
