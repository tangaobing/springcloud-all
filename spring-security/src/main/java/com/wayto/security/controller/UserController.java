package com.wayto.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: eureka-server
 * @description: user权限
 * @author: tgb
 * @create: 2020/11/03 10:47
 */

@RestController
@RequestMapping("/user")
public class UserController {


	@GetMapping ("/hi")
	public String Hi() {

		System.out.println("user ~~~~~~~");

		return "user hi";
	}
}
