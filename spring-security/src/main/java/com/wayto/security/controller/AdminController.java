package com.wayto.security.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: eureka-server
 * @description: user权限
 * @author: tgb
 * @create: 2020/11/03 10:47
 */

@RestController
@RequestMapping("/admin")
public class AdminController {

	@GetMapping ("/hi")
	// 两种权限都可以访问
	@Secured ({"ROLE_admin","ROLE_user"})
	public String Hi() {

		System.out.println("admin ~~~~~~~");

		return "admin hi";
	}
}
