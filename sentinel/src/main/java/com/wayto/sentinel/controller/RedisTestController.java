package com.wayto.sentinel.controller;

import org.redisson.api.*;
import org.redisson.client.protocol.ScoredEntry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * @ package_msg: com.wayto.sentinel
 * @author: 谭高兵
 * @ Date: 2024/8/11
 */
@RestController
@RequestMapping("redis")
public class RedisTestController {

    @Resource
    RedissonClient redisson;


    @GetMapping("getRedisKey")
    public Long getRedisKey() {
        RKeys keys = redisson.getKeys();
        return keys.count();
    }

    @GetMapping("setStr")
    public String setStr() {
        RBucket<String> bucket = redisson.getBucket("sentinel");
        bucket.set("001");
        return "OK";
    }
    @GetMapping("getStr")
    public String getStr() {
        RBucket<String> bucket = redisson.getBucket("sentinel");
        return bucket.get();
    }
}
