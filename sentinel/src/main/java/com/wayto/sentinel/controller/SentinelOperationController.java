package com.wayto.sentinel.controller;

import com.wayto.sentinel.service.SentinelOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ package_msg: com.wayto.sentinel.controller
 * @author: 谭高兵
 * @ Date: 2024/8/11
 */
@RestController
@RequestMapping("sentinel")
public class SentinelOperationController {

    @Autowired
    private SentinelOperationService operationService;

    @GetMapping(value = "/hello/{name}")
    public String apiHello(@PathVariable String name) {
        return operationService.sayHello(name);
    }

}
