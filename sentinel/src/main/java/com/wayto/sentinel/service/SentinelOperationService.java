package com.wayto.sentinel.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @ package_msg: com.wayto.sentinel.service
 * @author: 谭高兵
 * @ Date: 2024/8/11
 */

@Service
public class SentinelOperationService {

    @SentinelResource(value = "sayHello")
    public String sayHello(String name) {
        return "Hello, " + name;
    }
}
